"C:\Program Files\JetBrains\IntelliJ IDEA 2023.2.3\jbr\bin\java.exe" "-javaagent:C:\Program Files\JetBrains\IntelliJ IDEA 2023.2.3\lib\idea_rt.jar=50380:C:\Program Files\JetBrains\IntelliJ IDEA 2023.2.3\bin" -Dfile.encoding=UTF-8 -classpath "D:\school\Informatica\Anul 3\Semestrul 1\Principiile Implementarii Orientate spre Performanta\lab\piop-sample-gradle-project\Lab3\target\classes;C:\Users\xpoiz\.m2\repository\org\openjdk\jmh\jmh-core\1.37\jmh-core-1.37.jar;C:\Users\xpoiz\.m2\repository\net\sf\jopt-simple\jopt-simple\5.0.4\jopt-simple-5.0.4.jar;C:\Users\xpoiz\.m2\repository\org\apache\commons\commons-math3\3.6.1\commons-math3-3.6.1.jar;C:\Users\xpoiz\.m2\repository\junit\junit\4.13.2\junit-4.13.2.jar;C:\Users\xpoiz\.m2\repository\org\hamcrest\hamcrest-core\1.3\hamcrest-core-1.3.jar" org.example.Main
# JMH version: 1.37
# VM version: JDK 17.0.8.1, OpenJDK 64-Bit Server VM, 17.0.8.1+7-b1000.32
# VM invoker: C:\Program Files\JetBrains\IntelliJ IDEA 2023.2.3\jbr\bin\java.exe
# VM options: -javaagent:C:\Program Files\JetBrains\IntelliJ IDEA 2023.2.3\lib\idea_rt.jar=50380:C:\Program Files\JetBrains\IntelliJ IDEA 2023.2.3\bin -Dfile.encoding=UTF-8
# Blackhole mode: compiler (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 10 iterations, 10 s each, 10000 calls per op
# Measurement: 10 iterations, 10 s each, 100000 calls per op
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.example.MyBenchmark.testAdd

# Run progress: 0.00% complete, ETA 00:10:00
# Fork: 1 of 1
# Warmup Iteration   1: 5151.813 ms/op
# Warmup Iteration   2: 5083.876 ms/op
# Warmup Iteration   3: 4871.552 ms/op
# Warmup Iteration   4: 4737.617 ms/op
# Warmup Iteration   5: 5471.653 ms/op
# Warmup Iteration   6: 4712.728 ms/op
# Warmup Iteration   7: 5036.220 ms/op
# Warmup Iteration   8: 4953.417 ms/op
# Warmup Iteration   9: 4880.392 ms/op
# Warmup Iteration  10: 4275.242 ms/op
Iteration   1: 50774.625 ms/op
Iteration   2: 55230.279 ms/op
Iteration   3: 113465.252 ms/op
Iteration   4: 91159.601 ms/op
Iteration   5: 108821.369 ms/op
Iteration   6: 64934.592 ms/op
Iteration   7: 51927.330 ms/op
Iteration   8: 48911.695 ms/op
Iteration   9: 50168.268 ms/op
Iteration  10: 49057.688 ms/op


Result "org.example.MyBenchmark.testAdd":
  68445.070 ±(99.9%) 39154.580 ms/op [Average]
  (min, avg, max) = (48911.695, 68445.070, 113465.252), stdev = 25898.330
  CI (99.9%): [29290.490, 107599.650] (assumes normal distribution)


# JMH version: 1.37
# VM version: JDK 17.0.8.1, OpenJDK 64-Bit Server VM, 17.0.8.1+7-b1000.32
# VM invoker: C:\Program Files\JetBrains\IntelliJ IDEA 2023.2.3\jbr\bin\java.exe
# VM options: -javaagent:C:\Program Files\JetBrains\IntelliJ IDEA 2023.2.3\lib\idea_rt.jar=50380:C:\Program Files\JetBrains\IntelliJ IDEA 2023.2.3\bin -Dfile.encoding=UTF-8
# Blackhole mode: compiler (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 10 iterations, 10 s each, 10000 calls per op
# Measurement: 10 iterations, 10 s each, 100000 calls per op
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.example.MyBenchmark.testAverage

# Run progress: 33.33% complete, ETA 00:06:42
# Fork: 1 of 1
# Warmup Iteration   1: 4532.009 ms/op
# Warmup Iteration   2: 3624.150 ms/op
# Warmup Iteration   3: 3825.477 ms/op
# Warmup Iteration   4: 8233.830 ms/op
# Warmup Iteration   5: 6958.034 ms/op
# Warmup Iteration   6: 8033.957 ms/op
# Warmup Iteration   7: 8151.883 ms/op
# Warmup Iteration   8: 3895.909 ms/op
# Warmup Iteration   9: 3909.411 ms/op
# Warmup Iteration  10: 3719.627 ms/op
Iteration   1: 36464.852 ms/op
Iteration   2: 40396.935 ms/op
Iteration   3: 65267.196 ms/op
Iteration   4: 95455.921 ms/op
Iteration   5: 98600.079 ms/op
Iteration   6: 80633.768 ms/op
Iteration   7: 94152.080 ms/op
Iteration   8: 94347.872 ms/op
Iteration   9: 92444.423 ms/op
Iteration  10: 93345.979 ms/op


Result "org.example.MyBenchmark.testAverage":
  79110.910 ±(99.9%) 35624.962 ms/op [Average]
  (min, avg, max) = (36464.852, 79110.910, 98600.079), stdev = 23563.706
  CI (99.9%): [43485.949, 114735.872] (assumes normal distribution)


# JMH version: 1.37
# VM version: JDK 17.0.8.1, OpenJDK 64-Bit Server VM, 17.0.8.1+7-b1000.32
# VM invoker: C:\Program Files\JetBrains\IntelliJ IDEA 2023.2.3\jbr\bin\java.exe
# VM options: -javaagent:C:\Program Files\JetBrains\IntelliJ IDEA 2023.2.3\lib\idea_rt.jar=50380:C:\Program Files\JetBrains\IntelliJ IDEA 2023.2.3\bin -Dfile.encoding=UTF-8
# Blackhole mode: compiler (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 10 iterations, 10 s each, 10000 calls per op
# Measurement: 10 iterations, 10 s each, 100000 calls per op
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.example.MyBenchmark.testTop

# Run progress: 66.67% complete, ETA 00:03:21
# Fork: 1 of 1
# Warmup Iteration   1: 12628.076 ms/op
# Warmup Iteration   2: 11911.542 ms/op
# Warmup Iteration   3: 15407.955 ms/op
# Warmup Iteration   4: 11918.865 ms/op
# Warmup Iteration   5: 11764.228 ms/op
# Warmup Iteration   6: 11885.754 ms/op
# Warmup Iteration   7: 11659.710 ms/op
# Warmup Iteration   8: 8949.409 ms/op
# Warmup Iteration   9: 7158.800 ms/op
# Warmup Iteration  10: 7128.743 ms/op
Iteration   1: 70749.850 ms/op
Iteration   2: 70458.540 ms/op
Iteration   3: 70812.962 ms/op
Iteration   4: 70428.822 ms/op
Iteration   5: 72332.587 ms/op
Iteration   6: 77280.339 ms/op
Iteration   7: 75628.279 ms/op
Iteration   8: 75694.562 ms/op
Iteration   9: 75428.214 ms/op
Iteration  10: 73705.176 ms/op


Result "org.example.MyBenchmark.testTop":
  73251.933 ±(99.9%) 3955.636 ms/op [Average]
  (min, avg, max) = (70428.822, 73251.933, 77280.339), stdev = 2616.408
  CI (99.9%): [69296.297, 77207.569] (assumes normal distribution)


# Run complete. Total time: 00:10:03

REMEMBER: The numbers below are just data. To gain reusable insights, you need to follow up on
why the numbers are the way they are. Use profilers (see -prof, -lprof), design factorial
experiments, perform baseline and negative tests that provide experimental control, make sure
the benchmarking environment is safe on JVM/OS/HW level, ask for reviews from the domain experts.
Do not assume the numbers tell you what you want them to tell.

NOTE: Current JVM experimentally supports Compiler Blackholes, and they are in use. Please exercise
extra caution when trusting the results, look into the generated code to check the benchmark still
works, and factor in a small probability of new VM bugs. Additionally, while comparisons between
different JVMs are already problematic, the performance difference caused by different Blackhole
modes can be very significant. Please make sure you use the consistent Blackhole mode for comparisons.

Benchmark                Mode  Cnt      Score       Error  Units
MyBenchmark.testAdd      avgt   10  68445.070 ± 39154.580  ms/op
MyBenchmark.testAverage  avgt   10  79110.910 ± 35624.962  ms/op
MyBenchmark.testTop      avgt   10  73251.933 ±  3955.636  ms/op

Process finished with exit code 0
