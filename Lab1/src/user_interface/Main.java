package user_interface;

import calculator.Calculator;
import user_interface.console.ConsoleUserInterface;

public class Main {
    public static void main(String[] args) {
        UserInterface userInterface =
                new ConsoleUserInterface(new Calculator());
        userInterface.start();
    }
}