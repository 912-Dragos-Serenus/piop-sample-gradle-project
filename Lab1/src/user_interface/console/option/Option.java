package user_interface.console.option;

import java.util.Scanner;

public interface Option {
    String message();

    void read(Scanner scanner);

    double[] values();
}
