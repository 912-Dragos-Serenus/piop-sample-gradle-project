package user_interface.console.option.nullnary;

public class ExitOption extends NullnaryOption{
    @Override
    public String message() {
        return "Exit";
    }
}
