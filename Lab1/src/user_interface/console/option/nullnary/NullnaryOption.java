package user_interface.console.option.nullnary;

import user_interface.console.option.Option;

import java.util.Scanner;

public abstract class NullnaryOption implements Option {
    @Override
    public void read(Scanner scanner) {
        // nothing to read
    }

    @Override
    public double[] values() {
        return new double[0];
    }
}
