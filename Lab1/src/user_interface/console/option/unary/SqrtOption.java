package user_interface.console.option.unary;

public class SqrtOption extends UnaryOption{
    @Override
    public String message() {
        return "Get the square root of a number.";
    }
}
