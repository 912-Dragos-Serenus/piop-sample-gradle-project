package user_interface.console.option.unary;

import user_interface.console.option.Option;

import java.util.MissingResourceException;
import java.util.Scanner;

public abstract class UnaryOption implements Option {
    private Double n = null;

    @Override
    public abstract String message();

    @Override
    public void read(Scanner scanner) {
        System.out.print("Number: ");
        this.n = scanner.nextDouble();
    }

    @Override
    public double[] values() {
        if(this.n == null)
            throw new MissingResourceException(
                    "Number not read.",
                    "UnaryOption",
                    "n"
            );

        double[] result = new double[2];
        result[0] = this.n;

        return result;
    }
}
