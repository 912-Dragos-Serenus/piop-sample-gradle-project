package user_interface.console.option.binary;

public class MinimumOption extends BinaryOption{
    @Override
    public String message() {
        return "Get the minimum out of 2 numbers.";
    }
}
