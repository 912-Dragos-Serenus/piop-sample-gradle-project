package user_interface.console.option.binary;

import user_interface.console.option.Option;

import java.util.MissingResourceException;
import java.util.Scanner;

public abstract class BinaryOption implements Option {
    private Double n1 = null;

    private Double n2 = null;

    @Override
    public abstract String message();

    @Override
    public void read(Scanner scanner) {
        System.out.print("First number: ");
        this.n1 = scanner.nextDouble();
        System.out.print("Second number: ");
        this.n2 = scanner.nextDouble();
    }

    @Override
    public double[] values() {
        if (this.n1 == null)
            throw new MissingResourceException(
                    "First number not read.",
                    "BinaryOption",
                    "n1"
            );
        if (this.n2 == null)
            throw new MissingResourceException(
                    "Second number not read.",
                    "BinaryOption",
                    "n2"
            );

        double[] result = new double[2];
        result[0] = this.n1;
        result[1] = this.n2;

        return result;
    }
}
