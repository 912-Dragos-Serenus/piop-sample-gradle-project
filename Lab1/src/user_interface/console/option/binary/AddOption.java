package user_interface.console.option.binary;

public class AddOption extends BinaryOption{
    @Override
    public String message() {
        return "Add 2 numbers.";
    }
}
