package user_interface.console.option.binary;

public class MultiplyOption extends BinaryOption{
    @Override
    public String message() {
        return "Multiply 2 numbers.";
    }
}
