package user_interface.console.option.binary;

public class DivideOption extends BinaryOption{
    @Override
    public String message() {
        return "Divide 2 numbers.";
    }
}
