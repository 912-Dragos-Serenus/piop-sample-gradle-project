package user_interface.console.option.binary;

public class MaximumOption extends BinaryOption{
    @Override
    public String message() {
        return "Get the maximum out of 2 numbers.";
    }
}
