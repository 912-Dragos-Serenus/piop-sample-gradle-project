package user_interface.console.option.binary;

public class SubtractOption extends BinaryOption{
    @Override
    public String message() {
        return "Subtract 2 numbers.";
    }
}
