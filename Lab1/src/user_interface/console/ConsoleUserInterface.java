package user_interface.console;

import calculator.Calculator;
import user_interface.console.option.binary.AddOption;
import user_interface.console.option.binary.SubtractOption;
import user_interface.console.option.binary.MultiplyOption;
import user_interface.console.option.binary.DivideOption;
import user_interface.console.option.binary.MaximumOption;
import user_interface.console.option.binary.MinimumOption;
import user_interface.console.option.nullnary.ExitOption;
import user_interface.console.option.Option;
import user_interface.console.option.unary.SqrtOption;
import user_interface.UserInterface;

import java.util.Scanner;

public class ConsoleUserInterface implements UserInterface {
    private final Calculator calculator;

    private final Option[] options;

    private final Scanner scanner;

    public ConsoleUserInterface(Calculator calculator){
        this.calculator = calculator;

        this.options = new Option[8];
        this.options[0] = new ExitOption();
        this.options[1] = new AddOption();
        this.options[2] = new SubtractOption();
        this.options[3] = new MultiplyOption();
        this.options[4] = new DivideOption();
        this.options[5] = new MaximumOption();
        this.options[6] = new MinimumOption();
        this.options[7] = new SqrtOption();

        this.scanner = new Scanner(System.in);
    }

    private void printOptions() {
        for (int i = 0; i < this.options.length; ++i) {
            System.out.println(i + ". " + options[i].message());
        }
    }

    @Override
    public void start() {
        boolean running = true;

        while(running) {
            this.printOptions();

            System.out.print("Option: ");
            int index = this.scanner.nextInt();
            if(index < 0 || index >= this.options.length)
                throw new IndexOutOfBoundsException("Invalid option.");

            this.options[index].read(this.scanner);

            if (this.options[index] instanceof ExitOption)
                running = false;
            else if (this.options[index] instanceof AddOption)
                System.out.println(
                        this.calculator.add(this.options[index].values()[0],
                                this.options[index].values()[1])
                );
            else if (this.options[index] instanceof SubtractOption)
                System.out.println(this.calculator.subtract(
                        this.options[index].values()[0],
                        this.options[index].values()[1])
                );
            else if (this.options[index] instanceof MultiplyOption)
                System.out.println(this.calculator.multiply(
                        this.options[index].values()[0],
                        this.options[index].values()[1])
                );
            else if (this.options[index] instanceof DivideOption)
                System.out.println(this.calculator.divide(
                        this.options[index].values()[0],
                        this.options[index].values()[1])
                );
            else if (this.options[index] instanceof MaximumOption)
                System.out.println(this.calculator.max(
                        this.options[index].values()[0],
                        this.options[index].values()[1])
                );
            else if (this.options[index] instanceof MinimumOption)
                System.out.println(this.calculator.min(
                        this.options[index].values()[0],
                        this.options[index].values()[1])
                );
            else if (this.options[index] instanceof SqrtOption)
                System.out.println(this.calculator.sqrt(
                        this.options[index].values()[0])
                );
        }
    }
}
