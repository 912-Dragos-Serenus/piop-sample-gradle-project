package calculator;

public class Calculator {
    public double add (double n1, double n2) {
        return n1 + n2;
    }

    public double multiply (double n1, double n2) {
        return n1 * n2;
    }

    public double subtract (double n1, double n2) {
        return n1 - n2;
    }

    public double divide (double n1, double n2) {
        return n1 / n2;
    }

    public double max (double n1, double n2) {
        return Math.max(n1, n2);
    }

    public double min (double n1, double n2) {
        return Math.min(n1, n2);
    }

    public double sqrt (double n) {
        return Math.sqrt(n);
    }
}
