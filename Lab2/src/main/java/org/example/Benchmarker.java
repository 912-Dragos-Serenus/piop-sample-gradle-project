package org.example;

import org.example.repository.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Measurement(iterations = 5, batchSize = 100000)
@Warmup(iterations = 5, batchSize = 10000)
@Fork(1)
public class Benchmarker {

    @State(Scope.Thread)
    public static class BenchmarkState {
        public InMemoryRepository<Integer> repository;
        public Integer inElement;
        public Integer outElement;

        @Setup(Level.Invocation)
        public void setup() {
            repository = new HashSetBasedRepository<>();
            inElement = 1;
            outElement = 2;

            repository.add(inElement);
        }

        @TearDown
        public void tearDown() {
            repository = null;
            inElement = null;
            outElement = null;
        }
    }

    @Benchmark
    public void addTrue(BenchmarkState state) {
        state.repository.add(state.inElement);
    }

    @Benchmark
    public void addFalse(BenchmarkState state) {
        state.repository.add(state.outElement);
    }

    @Benchmark
    public void containsTrue(BenchmarkState state, Blackhole blackhole) {
        blackhole.consume(state.repository.contains(state.inElement));
    }

    @Benchmark
    public void containsFalse(BenchmarkState state, Blackhole blackhole) {
        blackhole.consume(state.repository.contains(state.outElement));
    }

    @Benchmark
    public void removeTrue(BenchmarkState state) {
        state.repository.remove(state.inElement);
    }

    @Benchmark
    public void removeFalse(BenchmarkState state) {
        state.repository.remove(state.outElement);
    }
}
