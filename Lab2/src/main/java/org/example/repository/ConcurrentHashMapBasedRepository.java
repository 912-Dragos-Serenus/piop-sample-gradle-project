package org.example.repository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {
    private final Map<T, Object> map;

    public ConcurrentHashMapBasedRepository() {
        map = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T value) {
        map.put(value, new Object());
    }

    @Override
    public boolean contains(T value) {
        return map.containsKey(value);
    }

    @Override
    public void remove(T value) {
        map.remove(value);
    }
}
