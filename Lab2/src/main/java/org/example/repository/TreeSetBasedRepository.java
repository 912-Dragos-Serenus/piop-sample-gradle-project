package org.example.repository;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T> implements InMemoryRepository<T> {
    private final Set<T> set;

    public TreeSetBasedRepository() {
        set = new TreeSet<>(Comparator.comparingInt(Object::hashCode));
    }

    @Override
    public void add(T value) {
        set.add(value);
    }

    @Override
    public boolean contains(T value) {
        return set.contains(value);
    }

    @Override
    public void remove(T value) {
        set.remove(value);
    }
}
