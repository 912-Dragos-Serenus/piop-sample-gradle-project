package org.example.repository;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository<T> implements InMemoryRepository<T> {
    private final Set<T> set;

    public HashSetBasedRepository() {
        set = new HashSet<>();
    }

    @Override
    public void add(T value) {
        set.add(value);
    }

    @Override
    public boolean contains(T value) {
        return set.contains(value);
    }

    @Override
    public void remove(T value) {
        set.remove(value);
    }
}
