package org.example.repository;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;

public class FastUtilIntOpenHashSetBasedRepository implements InMemoryRepository<Integer> {
    private final IntOpenHashSet set;

    public FastUtilIntOpenHashSetBasedRepository() {
        set = new IntOpenHashSet();
    }

    @Override
    public void add(Integer value) {
        set.add((int) value);
    }

    @Override
    public boolean contains(Integer value) {
        return set.contains((int) value);
    }

    @Override
    public void remove(Integer value) {
        set.remove((int) value);
    }
}
