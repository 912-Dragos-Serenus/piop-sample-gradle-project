package org.example.repository;

import org.eclipse.collections.impl.set.mutable.UnifiedSet;

public class EclipseUnifiedSetBasedRepository<T> implements InMemoryRepository<T> {
    private final UnifiedSet<T> set;

    public EclipseUnifiedSetBasedRepository() {
        set = new UnifiedSet<>();
    }

    @Override
    public void add(T value) {
        set.add(value);
    }

    @Override
    public boolean contains(T value) {
        return set.contains(value);
    }

    @Override
    public void remove(T value) {
        set.remove(value);
    }
}
